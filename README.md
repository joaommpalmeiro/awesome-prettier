<!--lint disable awesome-contributing awesome-github-->

# Awesome Prettier [![Awesome](https://awesome.re/badge.svg)](https://awesome.re)

> ✨ A curated list of resources for and about Prettier

Prettier is a multi-language code formatter. It is typically used for front-end/JavaScript projects. In addition, Prettier has an ecosystem of plugins and sharable configurations.

## Contents

- [Official](#official)
- [Configs](#configs)
- [Plugins](#plugins)

## Official

- [Prettier repo](https://github.com/prettier/prettier)
- [Prettier website](https://prettier.io/)

## Configs

### Prettier only

- [@azz/prettier-config](https://github.com/azz/prettier-config)
- [@egoist/prettier-config](https://github.com/egoist/prettier-config)
- [@feedzai/prettier-config](https://github.com/feedzai/prettier-config)
- [@github/prettier-config](https://github.com/github/prettier-config)
- [@joaopalmeiro/prettier-config](https://github.com/joaopalmeiro/prettier-config)
- [@will-stone/prettier-config](https://github.com/will-stone/dx/tree/main/packages/prettier-config)
- [@yuler/prettier-config](https://github.com/yuler/prettier-config)
- [@zestia/prettier-config](https://github.com/zestia/prettier-config)

### Prettier + ESLint

- [eslint-config-wesbos](https://github.com/wesbos/eslint-config-wesbos)

## Plugins

- [prettier-plugin-tailwindcss](https://github.com/tailwindlabs/prettier-plugin-tailwindcss)
