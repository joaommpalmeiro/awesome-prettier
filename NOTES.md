# Notes

- Guidelines:
  - https://github.com/sindresorhus/awesome/blob/main/create-list.md
  - https://github.com/sindresorhus/awesome/blob/main/pull_request_template.md
  - https://github.com/sindresorhus/awesome/blob/main/awesome.md
  - https://github.com/sindresorhus/awesome-electron/blob/main/readme.md?plain=1
- https://github.com/dar5hak/generator-awesome-list
- https://awesomelist.kminek.pl/marklink
- https://github.com/joaopalmeiro/awesome-vega/blob/main/readme.md?plain=1
- `npm install -D awesome-lint prettier`

## Development

```bash
nvm install && nvm use && node --version
```

```bash
npm install
```

Sort the items in each list alphabetically (except those in the `Contents` section).

```bash
npm run lint
```
